// JavaScript source code
app.directive('ngImage', function ($window) {
    return {
        scope: {
            show: "="
        },
        link: function (scope, element, attrs) {
            attrs.$observe('src', function (value) {
                if (!value)
                    return;

                var setManipulation = false;
                var src = value;
                if (attrs.height) {
                    src = src + "?h=" + attrs.height;
                    setManipulation = true;
                }
                if (attrs.width) {
                    var widthStr = "w=" + attrs.width;
                    if (setManipulation) {
                        src = src + "&" + widthStr;
                    } else {
                        src = src + "?" + widthStr;
                        setManipulation = true;
                    }
                }


                if (attrs.responsive != null) {
                    var img = new Image();
                    img.src = src;

                    img.onload = function () {
                        if ($window.innerWidth < img.width) {
                            if (attrs.width && !attrs.height) {
                                var change = img.width / attrs.width;
                                img.width = attrs.width;
                                img.height = img.height / change;
                            }
                            else if (!attrs.width && attrs.height) {
                                var change = img.height / attrs.height;
                                img.width = img.width / change;
                                img.height = attrs.height;
                            }
                            else if (attrs.width) {
                                img.width = attrs.width;
                            }
                            else if (attrs.height) {
                                img.height = attrs.height;
                            }

                            src = value + '?w=' + $window.innerHeight + '&h=' + img.height + '&mode=crop';

                            if (attrs.fixedTopleft != null) {
                                src = src + ";archor=topleft"
                            }
                            else if (attrs.fixedBottomleft != null) {
                                src = src + ";archor=bottomleft"
                            }
                            else if (attrs.fixedToprigth != null) {
                                src = src + ";archor=topright"
                            }
                            else if (attrs.fixedBottomright != null) {
                                src = src + ";archor=bottomright"
                            }
                            element.attr('src', src);
                        }
                    }
                }

                element.attr('src', src);
            });
        }
    }
});